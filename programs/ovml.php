<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


bab_functionality::includefile('Ovml/Function');
require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/frame.class.php';



/**
 * <OFPortletContainer id="" classname="" mode="static,locked,..." placeholder="Placeholder text" editplaceholder="Placeholder text in edit mode" description="Always visible description" initwith="containerId">
 */
class Func_Ovml_Function_PortletContainer extends Func_Ovml_Function
{
	public function toString()
	{
		$addon = bab_getAddonInfosInstance('portlets');
		if (!$addon || !$addon->isInstalled() || !$addon->isValid())
		{
			return bab_toHtml('<OFPortletContainer>: The portlet addon is not correctly installed or misses some dependencies.');
		}


		$containerId = null;
		$saveas = null;
		$modes = array();
		$classname = '';
		$placeholder = '';
		$editplaceholder = '';
		$description = '';
		$categories = '';
		$initwith = null;


		if (count($this->args)) {
			foreach ($this->args as $name => $value) {
				switch (mb_strtolower(trim($name))) {
					case 'id':
						$containerId = $value;
						break;

					case 'saveas':
						$saveas = $value;
						break;

					case 'mode':
						$modes = explode(',', $value);
						$modes = array_flip($modes);
						break;

					case 'classname':
						$classname = $value;
						break;

					case 'placeholder':
						$placeholder = $value;
						break;

					case 'editplaceholder':
						$editplaceholder = $value;
						break;

					case 'description':
						$description = $value;
						break;

					case 'categories':
						$categories = $value;
						break;

					case 'initwith':
						$initwith = $value;
						break;
				}
			}

			if (!isset($containerId)) {
				return '';
			}

			$containerConfiguration = portlets_getContainerConfiguration($containerId);

			$initWithPortlets = null;
			if (isset($initwith) && !$containerConfiguration->id)
			{
				require_once dirname(__FILE__).'/portletconfiguration.class.php';
				$set = new portlets_PortletConfigurationSet();
				$initWithPortlets = $set->select($set->container->is($initwith));
			}

			if (!$containerConfiguration->locked && isset($modes['locked']))
			{
				$containerConfiguration->locked = 1;
				$containerConfiguration->save();
			}

			if (isset($initWithPortlets))
			{
				foreach($initWithPortlets as $portletConfiguration)
				{
					$portletConfiguration->id = null;
					$portletConfiguration->createdOn = date('Y-m-d H:i:s');
					$portletConfiguration->user = $containerConfiguration->locked ? 0 : bab_getUserId();
					$portletConfiguration->container = $containerId;
					$portletConfiguration->save();
				}
			}


			$locked = false;
			if (isset($modes['locked']) || $containerConfiguration->locked) {
				$classname .= ' portlets-locked';
				$locked = true;
			}



			if (isset($modes['ajax'])) {
				$containerContent = '';
			} else {
				$W = bab_Widgets();
				$canvas = $W->HtmlCanvas();

				$containerLayout = portlets_getContainerLayout($containerId, $locked);
				$containerContent = $containerLayout->display($canvas);
			}


			if (!isset($modes['locked']) && $containerConfiguration->isLockable()) {
				$classname .= ' portlets-lockable';
			}
			if ($containerConfiguration->isSortable()) {
				$classname .= ' portlets-sortable';
			}
			if ($containerConfiguration->isConfigurable()) {
				$classname .= ' portlets-configurable';
			}


			$output = '<div class="bab-portlet-container bab-portlets-container-' . $containerId . ' ' . $classname . '">'
				. '<div class="portlets-container-description">' . bab_toHtml($description) . '</div>'
				. '<div class="portlets-container-placeholder">' . $placeholder . '</div>'
				. '<div class="portlets-container-editplaceholder">' . bab_toHtml($editplaceholder) . '</div>'
				. $containerContent
				. '</div>';

			$output .= '<script type="text/javascript">
					window.bab["portletcontainercategories-' . $containerId . '"] = "' . bab_toHtml($categories) . '";
				</script>';

			if (isset($saveas)) {
				$this->gctx->push($saveas, $output);
				return '';
			}

			return $output;
		}



	}


}

