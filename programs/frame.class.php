<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';

bab_Widgets()->includePhpClass('Widget_Frame');



class portlets_Frame extends Widget_Frame
{
	private $portlet;
	
	private $title = null;
	
	private $removable = false;
	private $configurable = false;
	
	/**
	 *
	 * @param int		$configuration
	 * @param string	$title			The title of the block (nul if no block title to display).
	 */
	public function __construct($configuration, $title = null)
	{
		parent::__construct('portlet_' . $configuration);
		$this->setTitle($title);
	}



	/**
	 * Sets the title of the frame.
	 *
	 * @param string $title		The title that should be displayed (or null for no title).
	 * @return portlets_Frame
	 */
	public function setTitle($title = null)
	{
		$this->title = $title;
		return $this;
	}
	

// 	/**
// 	 * Returns the title of the frame.
// 	 *
// 	 * @return string
// 	 */
// 	public function getTitle()
// 	{
// 		return $this->title;
// 	}
	
	
	public function setPortlet(portlet_PortletInterface $portlet)
	{
		$this->portlet = $portlet;
		return $this;
	}
	
	public function setConfigurable($configurable = true)
	{
		$this->configurable = $configurable;
		return $this;
	}
	
	public function setRemovable($removable = true)
	{
		$this->removable = $removable;
		return $this;
	}
	
	
	/**
	 * @return Widget_Item
	 */
	protected function toolBar()
	{
		$W = bab_Widgets();
		
		$toolbar = $W->Frame();
		$toolbar->addClass('portlet-toolbar');
		
		$definition = $this->portlet->getPortletDefinition();
		
		$toolbar->addItem($W->Title($definition->getName(), 4));
		$toolbar->addItem(
			$buttons = $W->FlowLayout()->addClass('portlet-buttons')
		);
		if ($this->configurable) {
			$buttons->addItem(
				$W->Button()->addClass('portlet-edit-settings')
			);
		}
		if ($this->removable) {
			$buttons->addItem(
				$W->Button()->addClass('portlet-close')->setTitle(portlets_translate('Remove this portlet from this container?'))
			);
		}
		
		return $toolbar;
	}



	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'portlet-frame';
		return $classes;
	}


	public function display(Widget_Canvas $canvas)
	{
		$W = bab_Widgets();
		
		if (bab_isUserLogged())
		{
			$this->addItem($this->toolBar());
		}
		$frame = $W->Frame();
		$frame->addItem($this->portlet);
		$frame->addClass('portlet-content');
		
		if ($this->title === '_') {
		    $definition = $this->portlet->getPortletDefinition();
		    $this->title = $definition->getName();
		}

		if ($this->title) {
			$this->addItem(
			    $W->Title($this->title, 2)
                    ->addClass('portlet-frame-title')
			);
		}
		$this->addItem($frame);

		return parent::display($canvas);
	}
}