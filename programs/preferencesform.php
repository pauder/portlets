<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';



/**
 * Get configuration form
 * @return Widget_Form
 */
function portlets_getPreferencesForm(portlet_PortletDefinitionInterface $portletDefinition)
{
    $W = bab_Widgets();
    $form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(.6, 'em'));
    $form->setReadOnly(false);
    $form->setName('preferences');

    $preferences = $portletDefinition->getPreferenceFields();

    $blockTitleSelect = $W->Select();
    $blockTitleSelect->setName('_blockTitleType');
    $blockTitleSelect->addOption('portlet', $portletDefinition->getName());
    $blockTitleSelect->addOption('custom', portlets_translate('Custom...'));

    $blockTitleLineEdit = $W->LineEdit();
    $blockTitleLineEdit->setName('_blockTitle');

    $blockWidthSelect = $W->Select();
    $blockWidthSelect->setName('_blockWidth');
    $blockWidthSelect->addOption('', '');
    $blockWidthSelect->addOption('col-md-12', '12 / 12');
    $blockWidthSelect->addOption('col-md-11', '11 / 12');
    $blockWidthSelect->addOption('col-md-10', '10 / 12');
    $blockWidthSelect->addOption('col-md-9', '9 / 12');
    $blockWidthSelect->addOption('col-md-8', '8 / 12');
    $blockWidthSelect->addOption('col-md-7', '7 / 12');
    $blockWidthSelect->addOption('col-md-6', '6 / 12');
    $blockWidthSelect->addOption('col-md-5', '5 / 12');
    $blockWidthSelect->addOption('col-md-4', '4 / 12');
    $blockWidthSelect->addOption('col-md-3', '3 / 12');
    $blockWidthSelect->addOption('col-md-2', '2 / 12');
    $blockWidthSelect->addOption('col-md-1', '1 / 12');

    $blockHeightLineEdit = $W->LineEdit();
    $blockHeightLineEdit->setName('_blockHeight');

    $blockClassnameLineEdit = $W->LineEdit();
    $blockClassnameLineEdit->setName('_blockClassname');


    $form->addItem(
        $W->LabelledWidget(
            portlets_translate('Block title'),
            $W->FlowItems(
                $blockTitleSelect,
                $blockTitleLineEdit
            )->setHorizontalSpacing(1, 'em')
        )
    );
    $form->addItem(
        $W->FlowItems(
            $W->LabelledWidget(
                portlets_translate('Block width'),
                $blockWidthSelect
            ),
            $W->LabelledWidget(
                portlets_translate('Block height'),
                $blockHeightLineEdit
            ),
            $W->LabelledWidget(
                portlets_translate('Block classname'),
                $blockClassnameLineEdit
            )
        )->setHorizontalSpacing(1, 'em')
    );

    $blockTitleSelect->setAssociatedDisplayable($blockTitleLineEdit, array('custom'));


    foreach ($preferences as $field) {
        $input = null;
        if(!isset($field['type'])){
            continue;
        }
        switch ($field['type']) {
            default:
            case 'text':
                $input = $W->LineEdit();
                break;

            case 'link':
                $input = $W->Link($field['text'], $field['link']);
                break;

            case 'int':
                $input = $W->RegExpLineEdit()
                    ->setRegExp('^[0-9]+$')
                    ->setSize(5);
                break;

            case 'boolean':
                $input = $W->Checkbox();
                break;

            case 'password':
                $input = $W->LineEdit()->obfuscate();
                break;

            case 'range':
                $input = $W->Select();
                for ($i = (int)$field['min']; $i <= (int)$field['max']; $i += $field['step']) {
                    $input->addOption($i, $i);
                }
                break;

            case 'list':
                $input = $W->Select();
                if (isset($field['options'])) {
                    foreach ($field['options'] as $opt) {
                        $group = isset($opt['group']) ? $opt['group'] : Widget_Select::NOGROUPKEY;
                        $input->addOption($opt['value'], $opt['label'], $group);
                    }
                }
                break;

            case 'multiselect':
                $input = $W->Multiselect();
                if (isset($field['options'])) {
                    foreach ($field['options'] as $opt) {
                        $group = isset($opt['group']) ? $opt['group'] : Widget_Select::NOGROUPKEY;
                        $input->addOption($opt['value'], $opt['label'], $group);
                    }
                }
                break;

            case 'hidden':
                // $input = $W->Hidden();
                continue 2;

            case 'idTopic':
                $input = $W->TopicPicker();
                break;

            case 'idCategory':
                $input = $W->TopicCategoryPicker();
                break;

            case 'idUser':
                bab_Functionality::includefile('Icons');
                $input = $W->UserPicker();
                $input->setSizePolicy(Func_Icons::ICON_LEFT_16);
                break;

            case 'date':
                $input = $W->DatePicker();
                break;

            case 'time':
                $input = $W->TimeEdit();
                break;

            case 'color':
                $input = $W->ColorPicker();
                break;

            case 'bab_file':
                $input = $W->BabFilePicker()
                    ->addClass('widget-fullwidth');
                break;

            case 'sitemapnode':
                $input = $W->SitemapItemPicker();
                break;

            case 'widget':
                $input = $field['item'];
        }

        if (isset($input) && isset($field['name'])) {
            $input->setName($field['name']);
        }

        if (isset($field['label'])) {
            $description = isset($field['description']) ? $field['description'] : null;
            $layout = $W->LabelledWidget($field['label'], $input, null, $description);
        } else {
            $layout = $input;
        }

        $form->addItem($layout);
    }

    return $form;
}
