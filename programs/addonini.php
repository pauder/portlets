; <?php/*
[general]
name                            ="portlets"
version	                        ="0.23.10"
addon_type                      ="EXTENSION"
encoding                        ="UTF-8"
mysql_character_set_database    ="latin1,utf8"
description                     ="Manage portlets on a portal"
description.fr                  ="Module de gestion de portlets sur le portail"
long_description.fr             ="README.md"
delete                          =1
ov_version                      ="7.6.0"
php_version                     ="5.1.0"
author                          ="Cantico"
icon                            ="icon.jpg"
configuration_page              ="admin"
addon_access_control            ="0"
tags                            ="extension,default,portlet"

[addons]

jquery=">=1.4.4"
widgets=">=1.0.61"
LibOrm=">=0.9.14"

;*/?>